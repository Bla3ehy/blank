﻿namespace WEBAPI.Utility
{
    public static class CacheUtility
    {
        public static void SetAllDataCache()
        {
            ObjectCache cache = MemoryCache.Default;

            cache.Set("All", new Product().GetProducts(), new CacheItemPolicy { SlidingExpiration = TimeSpan.FromMinutes(60) });
        }

        public static object GetCache(string key = "All")
        {
            ObjectCache cache = MemoryCache.Default;

            return cache.Get(key);
        }
    }
}

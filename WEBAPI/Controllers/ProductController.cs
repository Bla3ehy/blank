namespace WEBAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly ILogger<ProductController> _logger;
        private readonly ProductService _productService;


        public ProductController(ILogger<ProductController> logger, ProductService productService)
        {
            _logger = logger;
            _productService = productService;
        }

        /// <summary>
        /// 取得購物車清單
        /// </summary>
        /// <returns></returns>
        [HttpGet(Name = "GetShoppingCart")]
        public async Task<IActionResult> Get()
        {
            try
            {
                var products = await _productService.GetProductsAsync();

                return Ok(products);
            }
            catch (Exception e)
            {
                _logger.LogError($"{e}");

                return BadRequest("Something Error");
            }
            
        }
    }
}
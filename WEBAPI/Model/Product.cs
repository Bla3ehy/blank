﻿namespace WEBAPI.Model
{
    public class Product
    {
        /// <summary>
        /// 產品編號
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// 產品名稱
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 價格
        /// </summary>
        public decimal Price { get; set; }

        /// <summary>
        /// 產品效期開始日
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// 產品效期結束日
        /// </summary>
        public DateTime EndDate { get; set; }

        public List<Product> GetProducts()
        {
            return new List<Product>()
            {
                {new Product{Id = 1, Name = "Apple", Price = 60, StartDate = new DateTime(2023, 8, 11), EndDate = DateTime.Now.AddDays(1)}},
                {new Product{Id = 2, Name = "Orange", Price = 40, StartDate = new DateTime(2023, 8, 10), EndDate = DateTime.Now}},
            };
        }
    }
}

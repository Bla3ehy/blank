﻿namespace WEBAPI.Repository
{
    public interface IProductRepository
    {
        public Task<List<Product>> GetAllProductsAsync();
    }
}

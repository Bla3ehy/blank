﻿using WEBAPI.Utility;

namespace WEBAPI.Repository
{
    public class ProductRepository : IProductRepository
    {
        public async Task<List<Product>> GetAllProductsAsync()
        {
            return await Task.FromResult((List<Product>)CacheUtility.GetCache());
        }
    }
}

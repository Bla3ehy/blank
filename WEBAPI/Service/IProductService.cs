﻿namespace WEBAPI.Service
{
    public interface IProductService
    {
        public Task<IEnumerable<Product>> GetProductsAsync();
    }
}

﻿using System.Globalization;

namespace WEBAPI.Service
{
    public class ProductService : IProductService
    {
        private readonly ProductRepository _productRepository;

        public ProductService()
        {
            _productRepository = new ProductRepository();
        }

        public async Task<IEnumerable<Product>> GetProductsAsync()
        {
            var products = await _productRepository.GetAllProductsAsync();

            return products.Where(o => o.StartDate <= DateTime.ParseExact("20230811", "yyyyMMdd", CultureInfo.InvariantCulture));
        }
    }
}
